#include "qsysteminfoclient.h"
#include <QHostInfo>
#include <QNetworkInterface>
#include <QFile>
#include <QDebug>

QSystemInfoClient::QSystemInfoClient(QWidget *parent)
    : QWidget(parent)
{
	ui.setupUi(this);
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(slot_timer_timeout()));
    timer->start(10000); // Every 5 sec
    
    button = new QPushButton("System info");
    button->setIcon(QIcon(QString::fromUtf8(":/icons/default/48x48/gtk-info.png")));
    button->setIconSize(QSize(48, 48));
    button->setCheckable(true);
    button->setMinimumHeight(50);
    QFont font;
    font.setPointSize(24);
    button->setFont(font);
    slot_timer_timeout();
}

QSystemInfoClient::~QSystemInfoClient()
{
    delete timer;
}

void QSystemInfoClient::slot_timer_timeout()
{
    QHostInfo info = QHostInfo::fromName(QHostInfo::localHostName ());    
    QString ips;
    QString load;
    QString text;

    // discover network interfaces and addressEntries 
    foreach( QNetworkInterface interface, QNetworkInterface::allInterfaces() ){
        foreach( QNetworkAddressEntry addressEntry, interface.addressEntries() ){
            QString address = QHostAddress( addressEntry.ip() ).toString();
            int dots = address.count(".");
            if (dots) {
                ips += interface.name() + ": " + (addressEntry.ip()).toString() + "\n";
            }
        }
    }

    text += ips + "\n";
    
#ifndef Q_WS_WIN
    // This code is strictly platform specific
    QFile file;    
    QString cpu_average;
    QString memory;
    QString uptime;
    
    // Read average CPU load 
    file.setFileName("/proc/loadavg");
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QTextStream in(&file);
        cpu_average = in.readLine();
        file.close();                           
    }
    load = cpu_average.left(3);
    cpu_average = "CPU load: " + cpu_average;    
    
    // Read memory params
    file.setFileName("/proc/meminfo");
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QTextStream in(&file);
        memory = in.readLine() + "\n" + in.readLine();
        file.close();               
    }
    
    // Read system uptime
    file.setFileName("/proc/uptime");
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QTextStream in(&file);
        float seconds_float = (((in.readLine()).split(" ")).first()).toFloat(NULL);
        int hours = abs(seconds_float / 60 / 60);
        int minutes = abs((seconds_float - hours * (60*60))/60);
        int seconds = abs(seconds_float - hours * (60*60) - minutes * 60);
        uptime = "Uptime: " + QString::number(hours, 10) + "h. " + QString::number(minutes, 10) + "m. "  + QString::number(seconds, 10) + "s.";
        file.close();               
    }
    
    text += cpu_average + "\n" + memory + "\n" + uptime;    
#endif
    
    ui.label->setText(text);
    bool ok;
    text = QString::fromUtf8("");
    float load_float = load.toFloat(&ok);
    if (ok) {
        load_float *= 100;
        text.setNum(load_float, 'f', 0);
        text += "%";
    }

    button->setText(text);
}
