#ifndef QSYSTEMINFOCLIENT_H
#define QSYSTEMINFOCLIENT_H

#include <QPushButton>
#include <QTimer>
#include <QWidget>
#include "ui_qsysteminfoclient.h"

class QSystemInfoClient : public QWidget
{
    Q_OBJECT

public:
    QSystemInfoClient(QWidget *parent = 0);
    ~QSystemInfoClient();
    QPushButton * button;

private:
    Ui::QSystemInfoClientClass ui;
    QTimer * timer;
    
private slots:
    void slot_timer_timeout();    
};

#endif // QSYSTEMINFOCLIENT_H
