/*
 * QSimpleLogger.h
 *
 *  Created on: Nov 28, 2010
 *      Author: abrodkin
 */

#ifndef QSIMPLELOGGER_H_
#define QSIMPLELOGGER_H_


#include <QString>


void QSimpleLoggerInstall(QString app_name, QString app_version, bool write_file, QString filename = 0);
void QSimpleLoggerUninstall();


#endif /* QSIMPLELOGGER_H_ */
