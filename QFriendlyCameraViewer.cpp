/*
 * ip_camera_viewer.cpp
 *
 *  Created on: Oct 27, 2010
 *      Author: Alexey Brodkin
 */

#include "QFriendlyCameraViewer.h"
#include "qgismeteoclient.h"
#include "qipcameraclient.h"
#include "qdatetimeclient.h"
#include "qsysteminfoclient.h"
#include "qzwaveclient.h"

#include <QDebug>
#include <QSignalMapper>

#define WINDOW_WIDTH	800
#define WINDOW_HEIGHT	480
#define SIDEPANE_WIDTH	160

QFriendlyCameraViewer::QFriendlyCameraViewer(QWidget* parent)
    : QWidget(parent)
{
    stackedWidget = new QStackedWidget(this);

    QWidget *sidePaneWidget = new QWidget(this);
    sidePaneWidget->setFixedWidth(SIDEPANE_WIDTH);

    buttonsVerticalLayout = new QVBoxLayout(sidePaneWidget);
    buttonsVerticalLayout->setContentsMargins(0, 0, 0, 0);

    QHBoxLayout *mainLayout = new QHBoxLayout(this);
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setSpacing(0);
    mainLayout->addWidget(stackedWidget, 1);
    mainLayout->addWidget(sidePaneWidget);

    resize(WINDOW_WIDTH, WINDOW_HEIGHT);

    mapper = new QSignalMapper( this );  
    buttonGroup = new QButtonGroup(buttonsVerticalLayout);
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(slot_click_default_button()));
    connect(buttonGroup, SIGNAL(buttonClicked(QAbstractButton *)), this, SLOT(slot_timer_start(QAbstractButton *)));
    connect( mapper, SIGNAL( mapped( QWidget* ) ), stackedWidget, SLOT( setCurrentWidget( QWidget* ) ) );

    
    /*****************/
    /* Setup clients */
    /*****************/
    
    /* Date-Time */
    QDateTimeClient * datetime = new QDateTimeClient(this);
    setupClient(datetime, datetime->button);

    /* Weather forcast */
    QGismeteoClient * gismeteo = new QGismeteoClient(this, 26063);
    setupClient(gismeteo, gismeteo->button);

    /* Ip-camera */
    QIpCameraClient * ipcamera = new QIpCameraClient(this, "QFriendlyCameraViewer.ini");
    setupClient(ipcamera, ipcamera->button);
   
    /* system info */
    QSystemInfoClient * systeminfo = new QSystemInfoClient(this);
    setupClient(systeminfo, systeminfo->button);

    /* ZWave actions */
    QZWaveClient * zwave = new QZWaveClient(this, "QFriendlyCameraViewer.ini");
    setupClient(zwave, zwave->button);

    defaultButton = ipcamera->button;
    slot_click_default_button();
}

QFriendlyCameraViewer::~QFriendlyCameraViewer()
{
    delete timer;
    delete mapper;
    delete buttonGroup;
}

void
QFriendlyCameraViewer::setupClient(QWidget *client, QPushButton *button)
{
    if (!client) {
        qCritical("No client specified");
        return;
    }
    
    if (!button) {
        qCritical("No button specified for client of class %s", client->metaObject()->className());
        return;
    }
    
    stackedWidget->addWidget(client);
    buttonGroup->addButton(button);
    buttonsVerticalLayout->addWidget(button);
    mapper->setMapping(button, client);
    connect( button, SIGNAL( clicked() ), mapper, SLOT( map() ) );
    return;
}

void QFriendlyCameraViewer::slot_click_default_button()
{
    defaultButton->click();   
    defaultButton->setChecked(true);  
}

void QFriendlyCameraViewer::slot_timer_start(QAbstractButton * button)
{
    if (button != defaultButton) {
        timer->start(60 * 1000); // Milliseconds
    }
}
