/*
 * QConfigParser.h
 *
 *  Created on: Oct 27, 2010
 *      Author: abrodkin
 */

#ifndef QCONFIGPARSER_H_
#define QCONFIGPARSER_H_

#include <QFile>
#include <QSettings>

class QConfigParser : public QObject {
    Q_OBJECT
public:
    QConfigParser(QString file_name);
    ~QConfigParser();
    QString parameterValue(QString group, QString key);

private:
    QSettings * settings;
};


#endif /* QCONFIGPARSER_H_ */
