/*
 * QConfigParser.cpp
 *
 *  Created on: Oct 27, 2010
 *      Author: abrodkin
 */

#include "QConfigParser.h"
#include <QDebug>
#include <QFile>

QConfigParser::QConfigParser(QString file_name)
{
    if (!QFile::exists(file_name)){
        qWarning() << "File doesn't exist: " + file_name;
        Q_ASSERT_X(0, "ConfigParser", "file doesn't exist");
    }
    
    settings = new QSettings(file_name, QSettings::IniFormat);
    settings->setIniCodec("UTF-8");
    if (settings->status() != QSettings::NoError){
        qWarning() << "Ini file <" + file_name + "> processing error:" << settings->status();
        return;
    }
}

QConfigParser::~QConfigParser()
{
    delete settings;
}

QString QConfigParser::parameterValue(QString group, QString key)
{
    QString value;
    value = settings->value(group + "/" + key).toString();

    if (value.isEmpty()){
        qWarning() << "No pair [" + group + "] " + key + " found";
        return QString();
    }

    qDebug() << "[" + group + "] " + key << value;

    return value;
}
