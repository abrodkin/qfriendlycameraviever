#include <QApplication>
#include "QFriendlyCameraViewer.h"

#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include "QSimpleLogger.h"
#endif

#define APP_NAME        "QFriendlyCameraViewer"

int main(int argc, char *argv[])
{
    QLocale::setDefault(QLocale(QLocale::Russian,QLocale::RussianFederation));
    QApplication a(argc, argv);

#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
    QSimpleLoggerInstall(APP_NAME, VERSION, false);
#endif

    QFriendlyCameraViewer w;

#ifdef __arm__
    w.showFullScreen();
    a.setOverrideCursor(QCursor(Qt::BlankCursor));
#else
    w.show();
#endif

    int ret = a.exec();

#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
    QSimpleLoggerUninstall();
#endif

    return ret;
}

