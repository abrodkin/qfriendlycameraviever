#ifndef QIPCAMERACLIENT_H
#define QIPCAMERACLIENT_H

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QString>
#include <QTimer>
#include <QPixmap>
#include <QPushButton>
#include <QStackedWidget>
#include <QLabel>
#include <QEvent>
#include <QWidget>

#include "QConfigParser.h"
#include "qipcamera.h"

#define MONITORS_NUMBER     6
#define DEFAULT_PERIOD_MONITOR      500 // msecs
#define DEFAULT_PERIOD_ZOOMED       200 // msecs

typedef enum {
    VIEW_MODE_SPLIT = 0,
    VIEW_MODE_ZOOM,
    VIEW_MODES_NUMBER
} view_mode_t;

class QIpCameraClient : public QWidget
{
    Q_OBJECT
public:
    QIpCameraClient(QWidget *parent = 0, QString iniFilename = QString::null);
    ~QIpCameraClient();
    QPushButton * button;

private:
    void init_camera(int number, QLabel * widget);
    void splitMode();
    void zoomedMode(QLabel * monitor);
    QIpCamera * camera[MONITORS_NUMBER];
    QLabel * zoom_widget;
    QWidget *split_widget;
    QLabel * monitor_widget[MONITORS_NUMBER];
    QConfigParser * config_parser;
    int monitor_zoomed_number;
    int intervals[VIEW_MODES_NUMBER];
    view_mode_t mode;
    QStackedWidget *stackedWidget;

private slots:
    void set_pixmap(int index, QPixmap pixmap);
    void slot_buttonReleased(bool checked);
    
protected:
    bool eventFilter(QObject *object, QEvent *event);
};

#endif // QIPCAMERACLIENT_H
