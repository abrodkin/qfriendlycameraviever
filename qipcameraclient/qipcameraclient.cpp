/*
 * qipcameraclient.cpp
 *
 *  Created on: Oct 27, 2010
 *      Author: Alexey Brodkin
 */

#include "qipcameraclient.h"
#include <QDebug>
#include <QPixmap>
#include <QHBoxLayout>

QIpCameraClient::QIpCameraClient(QWidget *parent, QString iniFilename)
    : QWidget(parent)
{
    QString string;
    bool ok;
    int value;

    stackedWidget = new QStackedWidget(this);
    QVBoxLayout *mainLayout = new QVBoxLayout(this);
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setSpacing(0);
    mainLayout->addWidget(stackedWidget);
    zoom_widget = new QLabel(this);
    split_widget = new QWidget(this);
    stackedWidget->addWidget(zoom_widget);
    stackedWidget->addWidget(split_widget);
    monitor_widget[0] = new QLabel(this);
    monitor_widget[1] = new QLabel(this);
    monitor_widget[2] = new QLabel(this);
    monitor_widget[3] = new QLabel(this);
    monitor_widget[4] = new QLabel(this);
    monitor_widget[5] = new QLabel(this);


    QVBoxLayout *splitLayout = new QVBoxLayout(split_widget);
    splitLayout->setContentsMargins(0, 0, 0, 0);
    splitLayout->setSpacing(0);
    QWidget *monitorTopWidget = new QWidget(this);
    QWidget *monitorBottomWidget = new QWidget(this);
    splitLayout->addWidget(monitorTopWidget);
    splitLayout->addWidget(monitorBottomWidget);
    QHBoxLayout *monitorTopHLayout = new QHBoxLayout(monitorTopWidget);
    QHBoxLayout *monitorBottomHLayout = new QHBoxLayout(monitorBottomWidget);
    monitorTopHLayout->setContentsMargins(0, 0, 0, 0);
    monitorTopHLayout->setSpacing(0);
    monitorBottomHLayout->setContentsMargins(0, 0, 0, 0);
    monitorBottomHLayout->setSpacing(0);
    monitorTopHLayout->addWidget(monitor_widget[0]);
    monitorTopHLayout->addWidget(monitor_widget[1]);
    monitorTopHLayout->addWidget(monitor_widget[2]);
    monitorBottomHLayout->addWidget(monitor_widget[3]);
    monitorBottomHLayout->addWidget(monitor_widget[4]);
    monitorBottomHLayout->addWidget(monitor_widget[5]);

    monitor_zoomed_number = 0;
    mode = VIEW_MODE_SPLIT;
    
    config_parser = new QConfigParser(iniFilename);
    
    string = config_parser->parameterValue("common", "monitoring_period");
    value = string.toInt(&ok, 10);
    if (ok)   
        intervals[VIEW_MODE_SPLIT] = value;
    else
        intervals[VIEW_MODE_SPLIT] = DEFAULT_PERIOD_MONITOR;
        
    string = config_parser->parameterValue("common", "zoomed_period");
    value = string.toInt(&ok, 10);
    if (ok)  
        intervals[VIEW_MODE_ZOOM] = value;
    else 
        intervals[VIEW_MODE_ZOOM] = DEFAULT_PERIOD_ZOOMED;
    
    init_camera(0, monitor_widget[0]);
    init_camera(1, monitor_widget[1]);
    init_camera(2, monitor_widget[2]);
    init_camera(3, monitor_widget[3]);
    init_camera(4, monitor_widget[4]);
    init_camera(5, monitor_widget[5]);

    delete config_parser;

    zoom_widget->installEventFilter(this);

    button = new QPushButton();
    button->setText(QString::fromUtf8("Кам."));
    button->setCheckable(true);
    QFont font;
    font.setPointSize(24);
    button->setIcon(QIcon(QString::fromUtf8(":/icons/default/48x48/camera-web.png")));
    button->setFont(font);
    button->setIconSize(QSize(48, 48));
    connect(button, SIGNAL(toggled (bool)), this, SLOT(slot_buttonReleased(bool)));

    splitMode();
}

QIpCameraClient::~QIpCameraClient()
{
    for (int i=0; i<MONITORS_NUMBER; i++) {
        if (camera[i]) {
            delete camera[i];
            camera[i] = NULL;
        }
    }
}

void QIpCameraClient::init_camera(int number, QLabel * widget)
{
    QString string;
    QString url;
    
    camera[number] = NULL;
    
    string = config_parser->parameterValue("camera_" + QString::number(number), "enabled");
    if (string.toLower() == QString("yes")) {
        url = config_parser->parameterValue("camera_" + QString::number(number), "url");
        if (!url.isEmpty()){
            camera[number] = new QIpCamera(number, url);
            connect(camera[number], SIGNAL(newImage(const int &, QPixmap)), this, SLOT(set_pixmap(const int &, QPixmap)));
            if (!camera[number]->isInitialized()) {
                delete camera[number];
                camera[number] = NULL;                    
            }
            else {
                widget->installEventFilter(this);
            }
        }    
    }
}

void QIpCameraClient::set_pixmap(int index, QPixmap pixmap)
{
    switch (mode) {
        case VIEW_MODE_SPLIT:
            if (monitor_widget[index])
                monitor_widget[index]->setPixmap(pixmap.scaled(monitor_widget[index]->width(), monitor_widget[index]->height(), Qt::IgnoreAspectRatio, Qt::FastTransformation));
            break;

        case VIEW_MODE_ZOOM:
            if (index == monitor_zoomed_number) {
                zoom_widget->setPixmap(pixmap.scaled(zoom_widget->width(), zoom_widget->height(), Qt::IgnoreAspectRatio, Qt::FastTransformation));
            }
            break;

        default:
            Q_ASSERT_X(0, "set_picture", "Unexpected view mode");
    }
}


bool QIpCameraClient::eventFilter(QObject *object, QEvent *event)
{
    if (event->type() == QEvent::MouseButtonPress) {
        if (object == zoom_widget) {
            splitMode();
        }
        else {
            for (int i=0; i<MONITORS_NUMBER; i++) {
                if (object == monitor_widget[i]) {
                    zoomedMode(monitor_widget[i]);                    
                }
            }
        }
        return true;
    }

    // standard event processing
    return QObject::eventFilter(object, event);
}

void QIpCameraClient::zoomedMode(QLabel * monitor)
{
    mode = VIEW_MODE_ZOOM;
    
    /* Iterate through all monitor widgets */
    for (int i=0; i<MONITORS_NUMBER; i++) {
        if (monitor_widget[i]) {
            /* Select monitor to be zoomed */
            if (monitor_widget[i] == monitor){                
                monitor_zoomed_number = i;
                camera[i]->setPeriod(intervals[VIEW_MODE_ZOOM]);
                /* Force selected camera to update picture */
                camera[i]->start();
                zoom_widget->setPixmap((*monitor_widget[i]->pixmap()).scaled(zoom_widget->width(), zoom_widget->height(), Qt::KeepAspectRatio, Qt::FastTransformation));
                stackedWidget->setCurrentWidget(zoom_widget);
            }
            /* Stop other cameras to save resources */
            else {
                if (camera[i]) {
                    camera[i]->stop();
                }
            }
        }
    }
}

void QIpCameraClient::splitMode()
{
    mode = VIEW_MODE_SPLIT;        
    for (int i=0; i<MONITORS_NUMBER; i++) {
        if (camera[i]) {
            camera[i]->setPeriod(intervals[VIEW_MODE_SPLIT]);
            /* Force current camera to update picture */
            camera[i]->start();
        }
    }
    stackedWidget->setCurrentWidget(split_widget);
}

void QIpCameraClient::slot_buttonReleased (bool checked)
{
    if (!checked) {
        splitMode();
    }
    
    return;
}
