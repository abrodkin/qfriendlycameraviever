/*
 * ip_camera.cpp
 *
 *  Created on: Oct 27, 2010
 *      Author: Alexey Brodkin
 */

#include "qipcamera.h"
#include <QDebug>
#include <QPixmap>

QIpCamera::QIpCamera(int i, QString url_string)
{
    instance = i;
    inited = false;
    network_manager = new QNetworkAccessManager(this);
    connect(network_manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(slot_network_manager_finished(QNetworkReply*)));
    
    url = new QUrl(url_string);
    if (!url->isValid()) {
        qWarning() << "Invalid url" << url;
        return;
    }

    inited = true;
    active = false;

    timerNetworkTimeout = new QTimer(this);
    timerNetworkTimeout->setSingleShot(true);
    connect(timerNetworkTimeout, SIGNAL(timeout()), this, SLOT(slot_timerNetworkTimeout()));

    timerRefresh = new QTimer(this);
    timerRefresh->setSingleShot(true);
    connect(timerRefresh, SIGNAL(timeout()), this, SLOT(slot_timerRefresh()));
    slot_timerRefresh();
}

QIpCamera::~QIpCamera()
{
    delete timerRefresh;
    delete timerNetworkTimeout;
    delete url;
    delete network_manager;    
}

void QIpCamera::setPeriod(int period)
{
    timerRefresh->setInterval(period);
}

bool QIpCamera::isInitialized()
{
    return inited;
}

void QIpCamera::start()
{
    active = true;
    timerRefresh->stop();
    slot_timerRefresh();
}

void QIpCamera::stop()
{
    active = false;
    timerRefresh->stop();
}

void QIpCamera::slot_timerRefresh()
{
    QNetworkRequest request(*url);
    reply = network_manager->get(request);
    reply->ignoreSslErrors();
    timerNetworkTimeout->start(10*1000);
}

void QIpCamera::slot_timerNetworkTimeout()
{
    reply->abort();
    reply->deleteLater();
    if (active)
        timerRefresh->start();
}

void QIpCamera::slot_network_manager_finished(QNetworkReply * reply)
{
    QPixmap pixmap;
    
    timerNetworkTimeout->stop();

    if (reply->error() != QNetworkReply::NoError) {
        qWarning() << "Error in" << reply->url() << ":" << reply->errorString();
    }
    else
    if (reply->header(QNetworkRequest::ContentTypeHeader).toString() != QString("image/jpeg")) {
        qWarning() << "Unexpected content type:" << reply->header(QNetworkRequest::ContentTypeHeader).toString();
    }
    else {
        QByteArray jpeg_data = reply->readAll();            
        pixmap.loadFromData(jpeg_data);            
    }
    
    emit newImage(instance, pixmap);

    if (active)
        timerRefresh->start();

    reply->deleteLater();
    
    return;
}

