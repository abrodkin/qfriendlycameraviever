/*
 * qipcamera.h
 *
 *  Created on: Oct 27, 2010
 *      Author: Alexey Brodkin
 */

#ifndef QIPCAMERA_H_
#define QIPCAMERA_H_


#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QString>
#include <QTimer>
#include <QPixmap>
#include <QWidget>

class QIpCamera : public QObject {
    Q_OBJECT
public:
    QIpCamera(int i, QString url_string);
    ~QIpCamera();
    void setPeriod(int period);
    void stop();
    void start();
    bool isInitialized();

private:
    int instance;
    bool active;
    bool inited;
    QUrl * url;
    QNetworkAccessManager * network_manager;
    QTimer * timerRefresh;
    QTimer * timerNetworkTimeout;
    QNetworkRequest request;
    QNetworkReply * reply;

signals:
    void newImage(int i, QPixmap);
    
private slots:
    void slot_network_manager_finished(QNetworkReply * reply);
    void slot_timerRefresh();
    void slot_timerNetworkTimeout();
};

#endif /* QIPCAMERA_H_ */
