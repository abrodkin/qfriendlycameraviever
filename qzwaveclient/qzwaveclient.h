#ifndef QZWAVECLIENT_H
#define QZWAVECLIENT_H

#include <QHBoxLayout>
#include <QNetworkAccessManager>
#include <QPushButton>
#include <QSettings>
#include <QVBoxLayout>
#include <QWidget>

class QZWaveClient : public QWidget
{
	Q_OBJECT

public:
	QZWaveClient(QWidget *parent, QString config);
	QPushButton *button;

private:
	QSettings *settings;
	QVBoxLayout *mainLayout;
	QNetworkAccessManager *nam;
};

#endif // QZWAVECLIENT_H
