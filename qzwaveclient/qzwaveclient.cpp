#include <QDebug>
#include <QLabel>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QObject>
#include <QSettings>
#include <QFrame>

#include "qzwaveclient.h"

typedef enum {
	QzTypeSwitchBinaryBiStable = 0,
	QzTypeSwitchBinaryUniStable = 1,
	QzTypesNumber
} QzType;

#define BUTTON_WIDTH		100
#define BUTTON_HEIGHT		50

namespace {
	class QzSwitchBinaryBiStable: public QFrame
	{
		Q_OBJECT

	#define REPLY_STATUS_ON		"on"
	#define REPLY_STATUS_OFF	"off"

	#define BUTTON_ON_SIGN		"ВКЛ"
	#define BUTTON_OFF_SIGN		"выкл"

	public:
		QzSwitchBinaryBiStable(QWidget *parent, QSettings *settings,
				       QNetworkAccessManager *nam)
			: QFrame(parent)
		{
			this->nam = nam;
			this->setFrameStyle(QFrame::Panel | QFrame::Sunken);
			this->setLineWidth(2);
			url_on = new QUrl(settings->value("url_on").toString());
			if (!url_on->isValid()) {
				qWarning() << QString("Invalid URL: %1")
					      .arg(url_on->toString());
				return;
			}
			url_off = new QUrl(settings->value("url_off")
					   .toString());
			if (!url_off->isValid()) {
				qWarning() << QString("Invalid URL: %1")
					      .arg(url_off->toString());
				return;
			}
			url_status = new QUrl(settings->value("url_status")
					      .toString());
			if (!url_status->isValid()) {
				qWarning() << QString("Invalid URL: %1")
					      .arg(url_status->toString());
				return;
			}
			description = new QLabel(settings->value("description",
						 "ENTER_DESCRIPTION_TEXT")
						 .toString(), this);
			button = new QPushButton(this);
			button->setFixedWidth(BUTTON_WIDTH);
			button->setFixedHeight(BUTTON_HEIGHT);
			button->setCheckable(true);
			layout = new QHBoxLayout();
			layout->addWidget(button);
			layout->addWidget(description);
			setLayout(layout);
			connect(button, SIGNAL(clicked()), this,
				SLOT(slot_button_clicked()));
			sendRequest(url_status);
		}

	private:
		QUrl *url_on;
		QUrl *url_off;
		QUrl *url_status;
		QLabel *description;
		QPushButton *button;
		QHBoxLayout *layout;
		QNetworkAccessManager *nam;

		void
		sendRequest(QUrl *url)
		{
			qDebug() << QString("Sending request to %1")
				    .arg(url->toString());
			QNetworkReply *reply = nam->get(QNetworkRequest(*url));
			connect(reply, SIGNAL(finished()), this,
			        SLOT(slot_request_finished()));
		}

	private slots:
		void slot_button_clicked(void)
		{
			if (button->isChecked())
				sendRequest(url_on);
			else
				sendRequest(url_off);
		}

		void slot_request_finished(void)
		{
			QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
			QString answer;
			if (reply->error() == QNetworkReply::NoError) {
				answer = QString(reply->readAll());
				qDebug() << QString("Request is finished: %1")
					    .arg(reply->url().toString());
			}
			else
				qWarning() << QString("Error in %1: %2")
					      .arg(reply->url().toString())
					      .arg(reply->errorString());

			reply->deleteLater();
			qDebug() << QString("Reply: %1").arg(answer);
			if (answer.contains(REPLY_STATUS_ON,
					    Qt::CaseInsensitive)) {
				button->setChecked(true);
				button->setText(QString::fromUtf8(BUTTON_ON_SIGN));
			}
			else if (answer.contains(REPLY_STATUS_OFF,
						 Qt::CaseInsensitive)) {
				button->setChecked(false);
				button->setText(QString::fromUtf8(BUTTON_OFF_SIGN));
			}
		}
	};

	class QzSwitchBinaryUniStable: public QFrame
	{
		Q_OBJECT

	public:
		QzSwitchBinaryUniStable(QWidget *parent, QSettings *settings,
					QNetworkAccessManager *nam)
			: QFrame(parent)
		{
			this->nam = nam;
			this->setFrameStyle(QFrame::Panel | QFrame::Sunken);
			this->setLineWidth(2);
			url_on = new QUrl(settings->value("url_on").toString());
			if (!url_on->isValid()) {
				qWarning() << QString("Invalid URL: %1")
					      .arg(url_on->toString());
				return;
			}
			description = new QLabel(settings->value("description",
						 "ENTER_DESCRIPTION_TEXT")
						 .toString(), this);
			button = new QPushButton(this);
			button->setFixedWidth(BUTTON_WIDTH);
			button->setFixedHeight(BUTTON_HEIGHT);
			layout = new QHBoxLayout();
			layout->addWidget(button);
			layout->addWidget(description);
			setLayout(layout);
			connect(button, SIGNAL(clicked()), this,
				SLOT(slot_button_clicked()));
		}

	private:
		QUrl *url_on;
		QLabel *description;
		QPushButton *button;
		QHBoxLayout *layout;
		QNetworkAccessManager *nam;

		void
		sendRequest(QUrl *url)
		{
			qDebug() << QString("Sending request to %1")
				    .arg(url->toString());
			QNetworkReply *reply = nam->get(QNetworkRequest(*url));
			connect(reply, SIGNAL(finished()), this,
			        SLOT(slot_request_finished()));
		}

	private slots:
		void slot_button_clicked(void)
		{
			sendRequest(url_on);
		}

		void slot_request_finished(void)
		{
			QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
			QString answer;
			if (reply->error() == QNetworkReply::NoError) {
				answer = QString(reply->readAll());
				qDebug() << QString("Request is finished: %1")
					    .arg(reply->url().toString());
			}
			else
				qWarning() << QString("Error in %1: %2")
					      .arg(reply->url().toString())
					      .arg(reply->errorString());

			reply->deleteLater();
			qDebug() << QString("Reply: %1").arg(answer);
		}
	};
} /* namespace */

#include "qzwaveclient.moc"

QZWaveClient::QZWaveClient(QWidget *parent, QString config)
	: QWidget(parent)
{
	settings = new QSettings(config, QSettings::IniFormat, this);
	settings->setIniCodec("UTF-8");
	if (settings->status() != QSettings::NoError){
		qWarning() << QString("Cannot process configuration file %1 (error code: %2)")
				      .arg(config).arg(settings->status());
		return;
	}

	button = new QPushButton(this);
	button->setIcon(QIcon(QString::fromUtf8(":/icons/z-wave_logo.png")));
	button->setIconSize(QSize(104, 50));
	button->setCheckable(true);
	button->setMinimumHeight(50);

	nam = new QNetworkAccessManager(this);
	mainLayout = new QVBoxLayout(this);

	QStringList groups = settings->childGroups();

	foreach (QString group, groups) {
		bool ok;
		QzType type;
		QWidget *widget = NULL;
		qDebug() << "Group: " << group;
		if (!group.contains("zwave_"))
			continue;

		settings->beginGroup(group);
		type = (QzType)settings->value("type").toInt(&ok);
		if ((!ok) || (type >= QzTypesNumber))
			continue;
		switch(type) {
		case QzTypeSwitchBinaryBiStable:
			widget = new QzSwitchBinaryBiStable(this, settings, nam);
			break;

		case QzTypeSwitchBinaryUniStable:
			widget = new QzSwitchBinaryUniStable(this, settings, nam);
			break;

		default:
			break;
		}
		settings->endGroup();
		if (widget)
			mainLayout->addWidget(widget);
	}

	mainLayout->addStretch(1);
}
