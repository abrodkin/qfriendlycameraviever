/*
 * ip_camera_viewer.h
 *
 *  Created on: Oct 27, 2010
 *      Author: Alexey Brodkin
 */

#ifndef IP_CAMERA_VIEWER_H_
#define IP_CAMERA_VIEWER_H_


#include <QMainWindow>
#include <QSignalMapper>
#include <QTimer>
#include <QPushButton>
#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QHeaderView>
#include <QMainWindow>
#include <QStackedWidget>
#include <QVBoxLayout>
#include <QWidget>


class QFriendlyCameraViewer : public QWidget {
    Q_OBJECT
public:
    QFriendlyCameraViewer(QWidget *parent = 0);
    ~QFriendlyCameraViewer();    
    
private:
    void setupClient(QWidget *client, QPushButton *button);
    QButtonGroup * buttonGroup;
    QSignalMapper * mapper;
    QTimer * timer;
    QPushButton * defaultButton;
    QStackedWidget *stackedWidget;
    QVBoxLayout *buttonsVerticalLayout;

private slots:    
    void slot_click_default_button();
    void slot_timer_start(QAbstractButton * button);
};

#endif /* IP_CAMERA_VIEWER_H_ */
