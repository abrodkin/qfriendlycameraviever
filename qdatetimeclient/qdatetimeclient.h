#ifndef QDATETIMECLIENT_H
#define QDATETIMECLIENT_H

#include <QPushButton>
#include <QTimer>
#include <QWidget>
#include "ui_qdatetimeclient.h"

class QDateTimeClient : public QWidget
{
    Q_OBJECT

public:
    QDateTimeClient(QWidget *parent = 0);
    ~QDateTimeClient();
    QPushButton * button;

private:
    Ui::QDateTimeClientClass ui;
    QDateTime * datetime;
    QTimer * timer;
    
private slots:    
    void slot_timer_timeout();
};

#endif // QDATETIMECLIENT_H
