#include "qdatetimeclient.h"
#include <QTime>
#include <QDateTime>
#include <QDebug>

QDateTimeClient::QDateTimeClient(QWidget *parent)
    : QWidget(parent)
{
	ui.setupUi(this);
	ui.calendar->setNavigationBarVisible(false);
	ui.calendar->setSelectionMode(QCalendarWidget::NoSelection);
	ui.calendar->setGridVisible(true);
	ui.calendar->setFirstDayOfWeek(Qt::Monday);
	ui.calendar->setSelectedDate(QDate::currentDate());
	
    button = new QPushButton("DateTime");
    button->setCheckable(true);
    button->setMinimumHeight(80);
    QFont font;
    font.setPointSize(40);
    button->setFont(font);
    
    font.setPointSize(32);
    ui.labelClock->setFont(font);

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(slot_timer_timeout()));
    timer->start(1000);
    slot_timer_timeout();
}

QDateTimeClient::~QDateTimeClient()
{

}

void QDateTimeClient::slot_timer_timeout()
{
    QTime time = QTime::currentTime();
    QDate date = QDate::currentDate();    
    QString month;
    QString day;
    QString text;
    
    ui.calendar->setSelectedDate(QDate::currentDate());
    button->setText(time.toString("hh:mm"));
    
    switch (date.dayOfWeek()) {
        case 1: text += QString::fromUtf8("Пн"); break;
        case 2: text += QString::fromUtf8("Вт"); break;
        case 3: text += QString::fromUtf8("Ср"); break;
        case 4: text += QString::fromUtf8("Чт"); break;
        case 5: text += QString::fromUtf8("Пт"); break;
        case 6: text += QString::fromUtf8("Сб"); break;
        case 7: text += QString::fromUtf8("Вс"); break;
        default: 
            qCritical("Unexpected day of week: %d", date.dayOfWeek());
    }
    
    text += ", ";
    text += QString::number(date.day());
    text += " ";

    switch (date.month()) {
        case 1: text += QString::fromUtf8("Янв"); break;
        case 2: text += QString::fromUtf8("Фев"); break;
        case 3: text += QString::fromUtf8("Мар"); break;
        case 4: text += QString::fromUtf8("Апр"); break;
        case 5: text += QString::fromUtf8("Май"); break;
        case 6: text += QString::fromUtf8("Июн"); break;
        case 7: text += QString::fromUtf8("Июл"); break;
        case 8: text += QString::fromUtf8("Авг"); break;
        case 9: text += QString::fromUtf8("Сен"); break;
        case 10: text += QString::fromUtf8("Окт"); break;
        case 11: text += QString::fromUtf8("Ноя"); break;
        case 12: text += QString::fromUtf8("Дек"); break;
        default: 
            qCritical("Unexpected month number: %d", date.month());
    }
    
    text += " ";
    text += QString::number(date.year());
    ui.labelClock->setText(time.toString() + "\t" + text);
}
