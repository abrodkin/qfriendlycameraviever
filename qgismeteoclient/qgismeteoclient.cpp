/*
 * qgismeteoclient.cpp
 *
 *  Created on: Dec 14, 2010
 *      Author: abrodkin
 */

#include "qgismeteoclient.h"
#include <QFile>
#include <QDomDocument>
#include <QTimer>
#include <QGraphicsSvgItem>
#include <QPainter>
#include <QSvgRenderer>
#include <QDebug>

QGismeteoClient::QGismeteoClient(QWidget *parent, int locationId)
    : QWidget(parent)
{
    ui.setupUi(this);
    
    inited = false;
    network_manager = new QNetworkAccessManager(this);
    connect(network_manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(slot_network_manager_finished(QNetworkReply*)));
    
    QString url_string = QString("http://informer.gismeteo.ru/xml/") + QString::number(locationId) + QString("_1.xml");
    
    url = new QUrl(url_string);
    if (!url->isValid()) {
        qWarning() << "Invalid url" << url;
        return;
    }
    
    //qDebug() << url_string;
    
    inited = true;
    active = true;
    timer = new QTimer(this);
    timer->setSingleShot(true);
    connect(timer, SIGNAL(timeout()), this, SLOT(slot_timer_timeout()));
    slot_timer_timeout();
    
    button = new QPushButton("Weather");
    button->setCheckable(true);
    QFont font;
    font.setPointSize(24);
    button->setFont(font);
    button->setIconSize(QSize(48, 48));
}

void
QGismeteoClient::parseXmlReport (QByteArray xmlData)
{
    QDomDocument doc;
    
    if (!doc.setContent(xmlData)) {
        return;
    }

    QDomElement element = doc.documentElement();

    if(!element.isNull()) {        
        if (!element.isNull() && element.tagName() == QString::fromUtf8("MMWEATHER")) {
            element = element.firstChildElement();
            if (!element.isNull() && element.tagName() == QString::fromUtf8("REPORT")) {
                element = element.firstChildElement();
                if (!element.isNull() && element.tagName() == QString::fromUtf8("TOWN")) {
                    forecast.townIndex = element.attribute("index", "0").toInt(NULL, 10);
                    forecast.townName = element.attribute("sname", "Unknown");
                    element = element.firstChildElement();
                    int number = 0;
                    while (!element.isNull()) {
                        parseDayForecast(element, &forecast.day[number]);
                        element = element.nextSiblingElement();
                        number++;
                    }
                }                
            }
        }        
    }

    forecast.updateDateTime = QDateTime::currentDateTime();
    showForecast(&forecast);
}


QGismeteoClient::~QGismeteoClient()
{
}

void 
QGismeteoClient::parseDayForecast(QDomElement element, QGismeteoForecastDay * day) 
{
    if (!element.isNull()){
        day->day = element.attribute("day", "0").toInt(NULL, 10);
        day->month = element.attribute("month", "0").toInt(NULL, 10);
        switch (day->month) {
            case 1: day->monthString = QString::fromUtf8("Янв"); break;
            case 2: day->monthString = QString::fromUtf8("Фев"); break;
            case 3: day->monthString = QString::fromUtf8("Мар"); break;
            case 4: day->monthString = QString::fromUtf8("Апр"); break;
            case 5: day->monthString = QString::fromUtf8("Май"); break;
            case 6: day->monthString = QString::fromUtf8("Июн"); break;
            case 7: day->monthString = QString::fromUtf8("Июл"); break;
            case 8: day->monthString = QString::fromUtf8("Авг"); break;
            case 9: day->monthString = QString::fromUtf8("Сен"); break;
            case 10: day->monthString = QString::fromUtf8("Окт"); break;
            case 11: day->monthString = QString::fromUtf8("Ноя"); break;
            case 12: day->monthString = QString::fromUtf8("Дек"); break;
            default: break;
        }
        day->year = element.attribute("year", "0").toInt(NULL, 10);
        day->hour = element.attribute("hour", "0").toInt(NULL, 10);
        day->tod = element.attribute("tod", "0").toInt(NULL, 10);
        switch (day->tod) {
            case 0: day->todString = QString::fromUtf8("Ночь"); break;
            case 1: day->todString = QString::fromUtf8("Утро"); break;
            case 2: day->todString = QString::fromUtf8("День"); break;
            case 3: day->todString = QString::fromUtf8("Вечер"); break;
            default: break;
        }
        day->weekday = element.attribute("weekday", "0").toInt(NULL, 10);
        switch (day->weekday) {
            case 2: day->weekdayString = QString::fromUtf8("Пн"); break;
            case 3: day->weekdayString = QString::fromUtf8("Вт"); break;
            case 4: day->weekdayString = QString::fromUtf8("Ср"); break;
            case 5: day->weekdayString = QString::fromUtf8("Чт"); break;
            case 6: day->weekdayString = QString::fromUtf8("Пт"); break;
            case 7: day->weekdayString = QString::fromUtf8("Сб"); break;
            case 1: day->weekdayString = QString::fromUtf8("Вс"); break;
            default: break;
        }
        
        element = element.firstChildElement();
        while (!element.isNull()){
            if (element.tagName() == "PHENOMENA") {
                day->phenomena.cloudiness = (QGismeteoCloudness)element.attribute("cloudiness", "0").toInt();
                day->phenomena.precipitation = (QGismeteoPrecipitation)element.attribute("precipitation", "0").toInt();
                day->phenomena.rpower = element.attribute("rpower", "0").toInt();
                day->phenomena.spower = element.attribute("spower", "0").toInt();
            }
            else
            if (element.tagName() == "PRESSURE") {
                day->pressure.min = element.attribute("min", "0").toInt();
                day->pressure.max = element.attribute("max", "0").toInt();
            }
            else
            if (element.tagName() == "TEMPERATURE") {
                day->temperature.min = element.attribute("min", "0").toInt();
                day->temperature.max = element.attribute("max", "0").toInt();                
            }
            else
            if (element.tagName() == "WIND") {
                day->wind.direction = element.attribute("direction", "0").toInt();
                day->wind.speed.min = element.attribute("min", "0").toInt();
                day->wind.speed.max = element.attribute("max", "0").toInt();                
            }
            else
            if (element.tagName() == "RELWET") {
                day->relativeHumidity.min = element.attribute("min", "0").toInt();
                day->relativeHumidity.max = element.attribute("max", "0").toInt();                                
            }
            else
            if (element.tagName() == "HEAT") {
                day->heat.min = element.attribute("min", "0").toInt();
                day->heat.max = element.attribute("max", "0").toInt();                                
            }
            element = element.nextSiblingElement();
        }
    }
}

void QGismeteoClient::slot_timer_timeout()
{
    QNetworkRequest request(*url);
    network_manager->get(request);
}

void QGismeteoClient::slot_network_manager_finished(QNetworkReply * reply)
{    
    QByteArray xmlData;
    if (reply->error() != QNetworkReply::NoError) {
        qWarning() << "Error in" << reply->url() << ":" << reply->errorString();
    }
    else
    if (reply->header(QNetworkRequest::ContentTypeHeader).toString() != QString("text/xml")) {
        qWarning() << "Unexpected content type:" << reply->header(QNetworkRequest::ContentTypeHeader).toString();
    }
    else {
        xmlData = reply->readAll();                        
    }
    
    parseXmlReport(xmlData);

    if (active) {
        timer->setInterval(60 * 1000);
        timer->start();
    }

    reply->deleteLater();
    
    return;
}

void QGismeteoClient::showForecast(QGismeteoForecast * forecast)
{
    if (forecast) {        
        for (int i=0; i<4; i++){
            QString text;
            QString path;
            QLabel * data;
            QLabel * icon;
            QLabel * more_info;
            QLabel * temperature;
            QGismeteoForecastDay * day = &forecast->day[i];
            switch (i) {
                case 0: data = ui.lb_weather_data_0; 
                        icon = ui.lb_weather_icon_0;
                        more_info = ui.lb_weather_more_info_0;
                        temperature = ui.lb_weather_temperature_0;
                        break;
                case 1: data = ui.lb_weather_data_1; 
                        icon = ui.lb_weather_icon_1;
                        more_info = ui.lb_weather_more_info_1;
                        temperature = ui.lb_weather_temperature_1;
                        break;
                case 2: data = ui.lb_weather_data_2; 
                        icon = ui.lb_weather_icon_2;
                        more_info = ui.lb_weather_more_info_2;
                        temperature = ui.lb_weather_temperature_2;
                        break;
                case 3: data = ui.lb_weather_data_4; 
                        icon = ui.lb_weather_icon_4;
                        more_info = ui.lb_weather_more_info_4;
                        temperature = ui.lb_weather_temperature_4;
                        break;
                default: 
                    qFatal("Unexpected index"); 
            }
            
            text = day->weekdayString + ", " + QString::number(day->day) + " " + day->monthString + "\n" + day->todString;
            data->setText(text);
            text = QString::number((day->temperature.min + day->temperature.max)/2);
            temperature->setText(text);
            text += QString::fromUtf8("°");
            button->setText(text);
            
            text.clear();

            switch (day->phenomena.cloudiness) {
                case QGISMETEO_CLOUDNESS_FEW_CLOUDS:
                    text += QString::fromUtf8("малооблачно\n");
                    if ((day->tod == 1) || (day->tod == 2)) {
                        path = "weather-few-clouds.png";
                    }
                    else {
                        path = "weather-few-clouds-night.png";
                    }
                    break;

                case QGISMETEO_CLOUDNESS_DULL:
                    text += QString::fromUtf8("пасмурно\n");
                    if ((day->tod == 1) || (day->tod == 2)) {
                        path = "weather-clouds.png";
                    }
                    else {
                        path = "weather-clouds-night.png";
                    }
                    break;
                    
                case QGISMETEO_CLOUDNESS_CLOUDY:
                    text += QString::fromUtf8("облачно\n");
                    if ((day->tod == 1) || (day->tod == 2)) {
                        path = "weather-clouds.png";
                    }
                    else {
                        path = "weather-clouds-night.png";
                    }
                    break;

                default:
                case QGISMETEO_CLOUDNESS_SUNNY:
                    text += QString::fromUtf8("безблачно\n");
                    if ((day->tod == 1) || (day->tod == 2)) {
                        path = "weather-clear.png";
                    }
                    else {
                        path = "weather-clear-night.png";
                    }
            }

            switch (day->phenomena.precipitation) {
                case QGISMETEO_PRECIPITATION_SNOW:
                    text += QString::fromUtf8("небольшой\nснег\n");
                    path = "weather-snow.png";
                    break;
                    
                case QGISMETEO_PRECIPITATION_SNOW_2:
                    text += QString::fromUtf8("снег\n");
                    path = "weather-snow.png";
                    break;

                case QGISMETEO_PRECIPITATION_SWOWERS:
                    text += QString::fromUtf8("дождь\n");
                    path = "weather-showers-scattered.png";
                    break;
                    
                case QGISMETEO_PRECIPITATION_THUNDER:
                    text += QString::fromUtf8("гроза\n");
                    path = "weather-storm.png";
                    break;
                    
                case QGISMETEO_PRECIPITATION_STORM:
                    text += QString::fromUtf8("ливень\n");
                    path = "weather-showers.png";
                    break;
                
                default:
                    break;
            }                

            icon->setPixmap(QPixmap(":/icons/default/72x72/" + path));
            button->setIcon(QIcon(":/icons/default/48x48/" + path));
            
            text += QString::fromUtf8("\n");
            text += QString::number((day->pressure.max + day->pressure.min)/2) + QString::fromUtf8(" мм.\n");
            text += QString::number((day->relativeHumidity.max + day->relativeHumidity.min)/2) + QString::fromUtf8(" %\n");            
            more_info->setText(text);            
        }
    }
    
    QTime time = forecast->updateDateTime.time();
    QDate date = forecast->updateDateTime.date();    
    QString month;
    QString day;
    QString text = QString::fromUtf8("Обновлено в ");
    
    text += time.toString("hh:mm");
    text += " ";
    
    switch (date.dayOfWeek()) {
        case 1: text += QString::fromUtf8("Пн"); break;
        case 2: text += QString::fromUtf8("Вт"); break;
        case 3: text += QString::fromUtf8("Ср"); break;
        case 4: text += QString::fromUtf8("Чт"); break;
        case 5: text += QString::fromUtf8("Пт"); break;
        case 6: text += QString::fromUtf8("Сб"); break;
        case 7: text += QString::fromUtf8("Вс"); break;
        default: 
            qCritical("Unexpected day of week: %d", date.dayOfWeek());
    }
    
    text += ", ";
    text += QString::number(date.day());
    text += " ";

    switch (date.month()) {
        case 1: text += QString::fromUtf8("Янв"); break;
        case 2: text += QString::fromUtf8("Фев"); break;
        case 3: text += QString::fromUtf8("Мар"); break;
        case 4: text += QString::fromUtf8("Апр"); break;
        case 5: text += QString::fromUtf8("Май"); break;
        case 6: text += QString::fromUtf8("Июн"); break;
        case 7: text += QString::fromUtf8("Июл"); break;
        case 8: text += QString::fromUtf8("Авг"); break;
        case 9: text += QString::fromUtf8("Сен"); break;
        case 10: text += QString::fromUtf8("Окт"); break;
        case 11: text += QString::fromUtf8("Ноя"); break;
        case 12: text += QString::fromUtf8("Дек"); break;
        default: 
            qCritical("Unexpected month number: %d", date.month());
    }
    
    text += " ";
    text += QString::number(date.year());
    ui.labelUpdated->setText(text);
    
}
