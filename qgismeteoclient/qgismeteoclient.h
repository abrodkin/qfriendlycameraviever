#ifndef QGISMETEOCLIENT_H
#define QGISMETEOCLIENT_H

#include <QObject>
#include <QString>
#include <QDomElement>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QTimer>
#include <QDateTime>
#include <QPushButton>
#include <QWidget>
#include "ui_qgismeteoclient.h"

struct QRange {
    int min;
    int max;
};

struct QGismeteoWind {
    QRange speed;
    int direction;
};

typedef enum {
    QGISMETEO_CLOUDNESS_SUNNY = 0,      // Ясно
    QGISMETEO_CLOUDNESS_FEW_CLOUDS,     // Малооблачно
    QGISMETEO_CLOUDNESS_CLOUDY,         // Облачно
    QGISMETEO_CLOUDNESS_DULL            // Пасмурно
} QGismeteoCloudness;

typedef enum {
    QGISMETEO_PRECIPITATION_SWOWERS = 4,   // Дождь
    QGISMETEO_PRECIPITATION_STORM,      // Ливень
    QGISMETEO_PRECIPITATION_SNOW,       // Снег
    QGISMETEO_PRECIPITATION_SNOW_2,     // Снег
    QGISMETEO_PRECIPITATION_THUNDER,    // Гроза
    QGISMETEO_PRECIPITATION_NODATA,     // Нет данных
    QGISMETEO_PRECIPITATION_NO          // Без осадков
} QGismeteoPrecipitation;


struct QGismeteoPhenomena {
    QGismeteoCloudness cloudiness;    
    QGismeteoPrecipitation precipitation;
    int rpower;
    int spower;
};


struct QGismeteoForecastDay {
    int day;
    int month;
    QString monthString;
    int year;
    int hour;
    int tod;
    QString todString;
    int predict;  // заблаговременность прогноза в часах
    int weekday;
    QString weekdayString;
    QGismeteoPhenomena phenomena;
    QRange pressure;
    QRange temperature;
    QGismeteoWind wind;
    QRange relativeHumidity;
    QRange heat;
};

struct QGismeteoForecast {
    QString reportType;
    int townIndex;
    QString townName;
    QGismeteoForecastDay day[4];
    QDateTime updateDateTime;
};

class QGismeteoClient : public QWidget 
{
    Q_OBJECT
public:
    QGismeteoClient(QWidget *parent = 0, int locationIndex = 0);
    ~QGismeteoClient();
    QPushButton * button;

private:
    Ui::QGismeteoClientClass ui;
    QGismeteoForecast forecast;
    void parseDayForecast(QDomElement element, QGismeteoForecastDay * day);
    void parseXmlReport (QByteArray xmlData);
    void showForecast(QGismeteoForecast * forecast);
    
    bool active;
    bool inited;
    QUrl * url;
    QNetworkAccessManager * network_manager;
    QTimer * timer;
    QNetworkRequest request;
    

private slots:
    void slot_network_manager_finished(QNetworkReply * reply);
    void slot_timer_timeout();
};

/*
Описание формата:

    TOWN    информация о пункте прогнозирования:
      index уникальный пятизначный код города
      sname закодированное название города
      latitude  широта в целых градусах
     longitude  долгота в целых градусах
    FORECAST    информация о сроке прогнозирования:
      day, month, year  дата, на которую составлен прогноз в данном блоке
      hour  местное время, на которое составлен прогноз
      tod   время суток, для которого составлен прогноз: 0 - ночь 1 - утро, 2 - день, 3 - вечер
      weekday   день недели, 1 - воскресенье, 2 - понедельник, и т.д.
      predict   заблаговременность прогноза в часах
    PHENOMENA   атмосферные явления:
      cloudiness    облачность по градациям:  0 - ясно, 1- малооблачно, 2 - облачно, 3 - пасмурно
      precipitation тип осадков: 4 - дождь, 5 - ливень, 6,7 – снег, 8 - гроза, 9 - нет данных, 10 - без осадков
      rpower    интенсивность осадков, если они есть. 0 - возможен дождь/снег, 1 - дождь/снег
      spower    вероятность грозы, если прогнозируется: 0 - возможна гроза, 1 - гроза
    PRESSURE    атмосферное давление, в мм.рт.ст.
    TEMPERATURE температура воздуха, в градусах Цельсия
    WIND    приземный ветер
      min, max  минимальное и максимальное значения средней скорости ветра, без порывов
      direction     направление ветра в румбах, 0 - северный, 1 - северо-восточный,  и т.д.
    RELWET  относительная влажность воздуха, в %
    HEAT    комфорт - температура воздуха по ощущению одетого по сезону человека, выходящего на улицу

*/

#endif // QGISMETEOCLIENT_H
