include(QConfigParser/QConfigParser.pri)
lessThan(QT_MAJOR_VERSION, 5) {
	include(QSimpleLogger/QSimpleLogger.pri)
}
include(qipcameraclient/qipcameraclient.pri)
include(qgismeteoclient/qgismeteoclient.pri)
include(qdatetimeclient/qdatetimeclient.pri)
include(qsysteminfoclient/qsysteminfoclient.pri)
include(qzwaveclient/qzwaveclient.pri)

# Get version name from git repo information 
VER = $$system(git describe --always)
VERSION_STR = '\\"$$VER\\"'
DEFINES += VERSION=\"$$VERSION_STR\"

QT += core gui network svg xml

CONFIG += debug_and_release console
CONFIG(debug, debug|release) {
	TARGET	= QFriendlyCameraViewer
} else {
	TARGET	= QFriendlyCameraViewer-$$VER
}
# For Windows prefer static build so no need to have Qt/mingw .dlls
win32:CONFIG += static

TEMPLATE = app
SOURCES += QFriendlyCameraViewer.cpp main.cpp
HEADERS += QFriendlyCameraViewer.h

RESOURCES = icons/icons.qrc
