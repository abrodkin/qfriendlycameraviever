#!/bin/sh

# Start script for "ip_camera_viewer"
# 
# Required env variables are set here to 
# start "ip_camera_viewer" from init script


export TSLIB_TSDEVICE=/dev/input/event1
export TSLIB_TSEVENTTYPE=INPUT
export TSLIB_CONFFILE=/etc/ts.conf
export TSLIB_CALIBFILE=/etc/pointercal
export TSLIB_PLUGINDIR=/usr/lib/ts
export QTDIR=/usr/lib
export QT_QWS_FONTDIR=/usr/lib/fonts
export QWS_MOUSE_PROTO="TSLIB:/dev/input/event1"
export QWS_DISPLAY="LinuxFB:mmWidth210:mmHeight127:0"
./ip_camera_viewer -qws -fn Ubuntu -bg black -fg white -btn gray

